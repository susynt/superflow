Superflow
=============
A SusyNtuple looper for producing flat ntuples. 
Faciliates adding selections, variables, and systematic variations.

# Pre-Requisites
Setup the SusyNt packages needed to read SusyNtuples (see [UCINtSetup](https://gitlab.cern.ch/alarmstr/UCINtSetup))

# Setup Superflow package
It is assumed that the top level directory structure for the project contains a `source\`, `build\`, and `run\` directory as discussed in the [ATLAS Software Tutorial ](https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/).
From within this top level directory, do the following things
```bash
cd source
git clone ssh://git@gitlab.cern.ch:7999/susynt/superflow.git
cd ..
```

# Using Superflow

## Test run

Run `SuperflowAna -h` to see executable options
```bash
SuperflowAna -i path/to/SusyNt_file.root -c
```
# Running Benchmark Tests

Benchmark results from running over SusyNt samples for data15, data16, data17, data18, mc16a, mc16d, and mc16e are stored in [test/benchmarks](test/benchmarks).
The [README](test/benchmarks/README.md) provides details on how to replicate the benchmark tests. 
For developers, it is recommend the full set of tests be replicated and logged when committing changes that cause the benchmark tests to fail.

## Making your own superflow looper

TODO

